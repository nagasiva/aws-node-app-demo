FROM node:10-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json and package-lock.json are copied
COPY package*.json ./


# For production use npm install --only=production
RUN npm install

# Bundle app source
COPY . .

EXPOSE 3000
CMD ["npm","test"]