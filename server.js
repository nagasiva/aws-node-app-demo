/*
  main entry file
  entry file for serving the express app
*/

// dotenv import must be the first import
import 'dotenv/config';

const app = require('./src/app.js');

app.listen(process.env.PORT, () => console.log(`server is listening on port:${process.env.PORT}`));
