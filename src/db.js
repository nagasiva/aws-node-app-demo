/*
  entry file for mongodb
*/

import 'dotenv/config';

const mongoose = require('mongoose');

mongoose.connect(process.env.DB_CONNECTION_STRING, {
  useNewUrlParser: true,
}, () => {
  console.log('connected  to db');
});
