/*
  Entry file for express app
*/

const express = require('express');

const app = express();

app.use('/', (req, res) => {
  res.status(200).send('Hello World!');
});

module.exports = app;
