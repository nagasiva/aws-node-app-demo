import 'dotenv/config';

const mongoose = require('mongoose');

describe('setup the db connection', () => {
  beforeAll(() => {
    mongoose.connect(process.env.DB_CONNECTION_STRING);
  });
  test('test the db connection', () => {
    expect(mongoose.connection.readyState).toBe(2);
  });
  afterAll((done) => {
    mongoose.disconnect(done);
  });
});
